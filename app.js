var createError = require('http-errors');
var express = require('express');
var cookieParser = require('cookie-parser');
var cors = require('cors');
require('dotenv').config();
const swaggerJSDoc = require('swagger-jsdoc');
const swaggerUi = require('swagger-ui-express');
const path= require('path');
const bodyParser = require('body-parser');

// establish global connection
var db = require('./middlewear/database');
db.getPool();

var app = express();

// node environment file
mode = process.env.NODE_ENV || 'dev';

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

// mode can be access anywhere in the project
const config = require('config').get(mode);
app.use(express.json({ limit: '50mb' }));
app.use(bodyParser.json({limit: '50mb'}));
app.use(express.urlencoded({ limit: '50mb', extended: true }));
app.use(cookieParser());
app.use(cors())

const swaggerDefinition = {
  openapi: '3.0.0',
  info: {
    title: 'Hypermatrix Api Documentation',
    version: '1.0.0',
  },
  servers: [
    {
      url: config.serverUrlWebUrlLink,
      description: 'development server',
    }
  ]
};

const options = {
  swaggerDefinition,
  // Paths to files containing OpenAPI definitions
  apis: ['./routes/authentication/*.js'],
};
const swaggerSpec = swaggerJSDoc(options);
app.use('/docs', swaggerUi.serve, swaggerUi.setup(swaggerSpec));

//api calls to main router
const routeApis=require('./routes/route.js')
app.use(routeApis);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

app.listen(config.port, function () {
  console.log('Example app listening on port ' + config.port + '!')
});