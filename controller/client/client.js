const db_helper=require(`../../middlewear/database`);
const aws_helper=require(`../../middlewear/aws`);
const format = require("pg-format");

/**
 * get client details 
 */
 async function getClientsDetails(){

}

/**
 * post client details 
 */
 async function postClientsDetails(){

}

/**
 * put client details 
 */
 async function putClientsDetails(){

}

/**
 * change client status
 */
 async function statusChangeClients(){

}

module.exports={
    postClientsDetails,
    getClientsDetails,
    putClientsDetails,
    statusChangeClients
}