var database_util = require('../../middlewear/database');
var format = require('pg-format');

async function getLookup(key) {

    var splited_value = key.split(',');

    // console.log('Lookup Key splitted', splited_value)

    const lookupdict = {};

    for (i in splited_value) {
        if (splited_value[i] == 'country') {
            lookupdict['country'] = await getCountry()
        }
        if (splited_value[i] == 'state') {
            lookupdict['state'] = await getState()
        }
        if (splited_value[i] == 'city') {
            lookupdict['city'] = await getCity()
        }
        if (splited_value[i] == 'type_of_hash') {
            lookupdict['type_of_hash'] = await getTypeOfHash()
        }
        if (splited_value[i] == 'common_date_formate') {
            lookupdict['common_date_formate'] = await getCommonDateFormat()
        }
        if (splited_value[i] == 'time_zone') {
            lookupdict['time_zone'] = await getTimezone()
        }
        if (splited_value[i] == 'client_status') {
            lookupdict['client_status'] = await getClientStatus()
        }

    }
    return lookupdict;
}

async function getCountry(){
    var query = format(`select 
                        "CountryId", 
                        "CountryName" 
                    from 
                        public.country`)
    var result = await database_util.execute_query_params_return_query_result(
        query
    );
    if (result.rows.length > 0) {
        return result.rows[0];
    }
    return {}
}


async function getState(){
    var query =format(`select 
                        "StateId", 
                        "StateName" 
                    from 
                        public.state`)
    var result = await database_util.execute_query_params_return_query_result(
        query
    );
    if (result.rows.length > 0) {
        return result.rows[0];
    }
    return {}
}


async function getCity(){
    var query = format(`select 
                        "CityId", 
                        "CityName" 
                    from 
                        public.city`)
    var result = await database_util.execute_query_params_return_query_result(
        query
    );
    if (result.rows.length > 0) {
        return result.rows[0];
    }
    return {}
}


async function getTypeOfHash(){
    var query =format(`select 
                        "HashId", 
                        "HashName" 
                    from 
                        public."typeOfHash"`)
    var result = await database_util.execute_query_params_return_query_result(
        query
    );
    if (result.rows.length > 0) {
        return result.rows[0];
    }
    return {}
}


async function getCommonDateFormat(){
    var query = format(`select 
                        "commonDateFormatID", 
                        "commonDateName" 
                    from 
                        public."commonDateFormat"`)
    var result = await database_util.execute_query_params_return_query_result(
        query
    );
    if (result.rows.length > 0) {
        return result.rows[0];
    }
    return {}
}


async function getTimezone(){
    var query =format(`select 
                        "timezoneId", 
                        "timezoneName" 
                    from 
                        public.timezone`)
    var result = await database_util.execute_query_params_return_query_result(
        query
    );
    if (result.rows.length > 0) {
        return result.rows[0];
    }
    return {}
}



async function getClientStatus(){
    var query =format(`select 
                        "clientStatusId", 
                        "clientStatusName" 
                    from 
                        public."clientStatus"`)
    var result = await database_util.execute_query_params_return_query_result(
        query
    );
    if (result.rows.length > 0) {
        return result.rows[0];
    }
    return {}
}



module.exports = {
    getCountry,
    getState,
    getCity,
    getTypeOfHash,
    getCommonDateFormat,
    getTimezone,
    getClientStatus,
    getLookup

}
