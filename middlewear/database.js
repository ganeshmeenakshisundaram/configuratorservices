const { Pool } = require('pg');
// node environment file
mode = process.env.NODE_ENV || 'dev';
// mode can be access anywhere in the project
const config = require('config').get(mode);

let mainPool = null;
async function createPool() {
  const pool = await new Pool({
    host: config.database.host,
    port: config.database.port,
    user: config.database.user,
    password: config.database.password,
    database: config.database.database
  });
  return pool;
}

async function getPool() {
  if (!mainPool) {
    mainPool = await createPool();
    console.log("connection succesful");
  } else {
    console.log("already exists");
  }
}

async function execute_query_params_return_query_result(query, params) {
  // get connection from pool & execute query & release connections
  const client = await mainPool.connect();
  let results = [];
  try {
    await client.query('BEGIN');
    const res = await client.query(query, params);
    results = res;
    await client.query('COMMIT')
  } catch (e) {
    await client.query('ROLLBACK')
    throw e
  } finally {
    // Make sure to release the client before any error handling,
    // just in case the error handling itself throws an error.
    client.release();
  }
  return results;
}

async function execute_query_return_query_result(query) {
  // get connection from pool & execute query & release connections
  const client = await mainPool.connect();
  let results = [];
  try {
    const res = await client.query(query);
    results = res;
  } finally {
    // Make sure to release the client before any error handling,
    // just in case the error handling itself throws an error.
    client.release();
  }
  return results;
}

module.exports = {
  execute_query_params_return_query_result,
  execute_query_return_query_result,
  getPool
}