const fs = require('fs');
const AWS = require('aws-sdk');

async function UploadFile(name, file, file_type) {
    namesplit = name.split(".")
    var ln = namesplit.length
    // Read content from the file
    const fileContent = fs.readFileSync(file);
    name = namesplit[0] + Math.floor(new Date().getTime() / 1000).toString() + "." + namesplit[ln - 1];
    // s3 upload object
    const s3 = new AWS.S3({
        accessKeyId: process.env.AWS_ACCESS_KEY,
        secretAccessKey: process.env.AWS_SECRET_KEY
    });

    // Setting up S3 upload parameters
    const params = {
        Bucket: process.env.AWS_S3_BUCKET_NAME,
        Key: name, // File name you want to save as in S3
        Body: fileContent,
        ACL: 'public-read',
        ContentType: file_type
    };

    // Uploading files to the bucket
    let url = null;
    await new Promise((res, rej) => {
        s3.upload(params, function (err, data) {
            if (err) {
                console.log(err)
                rej(err)
            }
            url = data.Location;
            console.log(`File uploaded successfully.` + url);
            res()
        });
    });
    console.log(url)
    return url;
}

async function Getobject(Key) {
    // s3 upload object
    const s3 = new AWS.S3({
        accessKeyId: process.env.aws_access_key,
        secretAccessKey: process.env.aws_secret_key
    });

    // Uploading files to the bucket
    const data = await s3.getObject(
        {
            Bucket: process.env.AWS_S3_BUCKET_NAME,
            Key: Key
        }
    ).promise();
    return data.Body.toString('base64');
}

async function DeleteFile(file_name, folder_name) {
    // s3 upload object
    const s3 = new AWS.S3({
        accessKeyId: process.env.aws_access_key,
        secretAccessKey: process.env.aws_secret_key
    });
    if (file_name) {
        const delResp = await s3.deleteObject({
            Bucket: process.env.AWS_S3_BUCKET_NAME + '/' + folder_name,
            Key: file_name,
        }, async (err, data) => {
            if (err) {
                console.log("Error: Object delete failed.", err);
                return err
            }
            else {
                console.log("Success: Object delete successful.");
                return data
            }
        });
        return delResp;
    }
}

//add file to given folder path
async function UploadFileToFolder(name, file, file_type, folder_name) {
    namesplit = name.split(".")
    var ln = namesplit.length
    // Read content from the file
    const fileContent = fs.readFileSync(file);
    name = namesplit[0] + Math.floor(new Date().getTime() / 1000).toString() + "." + namesplit[ln - 1];
    // s3 upload object
    const s3 = new AWS.S3({
        accessKeyId: process.env.AWS_ACCESS_KEY,
        secretAccessKey: process.env.AWS_SECRET_KEY
    });

    // Setting up S3 upload parameters
    const params = {
        Bucket: process.env.AWS_S3_BUCKET_NAME + '/' + folder_name,
        Key: name, // File name you want to save as in S3
        Body: fileContent,
        ACL: 'public-read',
        ContentType: file_type
    };

    // Uploading files to the bucket
    let url = null;
    await new Promise((res, rej) => {
        s3.upload(params, function (err, data) {
            if (err) {
                console.log(err)
                rej(err)
            }
            url = data.Location;
            console.log(`File uploaded successfully.` + url);
            res()
        });
    });
    console.log(url)
    return url;
}

async function UploadBase64Format(name, fileContent, file_type,folder_name) {
    namesplit = name.split(".")
    var ln = namesplit.length

    name = namesplit[0].replace(/(\s+)/g, '_') + Math.floor(new Date().getTime() / 1000).toString() + "." + namesplit[ln - 1];
    const s3 = new AWS.S3({
        accessKeyId: process.env.AWS_ACCESS_KEY,
        secretAccessKey: process.env.AWS_SECRET_KEY
    });
    // Setting up S3 upload parameters
    const params = {
        Bucket: process.env.AWS_S3_BUCKET_NAME + '/' + folder_name,
        Key: name, // File name you want to save as in S3
        Body: fileContent,
        ACL: 'public-read',
        ContentType: file_type
    };

    // Uploading files to the bucket
    let url = null;
    await new Promise((res, rej) => {
        s3.upload(params, function (err, data) {
            if (err) {
                console.log(err)
                rej(err)
            }
            url = data.Location;
            console.log(`File uploaded successfully.` + url);
            res()
        });
    });
    return url;
}

module.exports = {
    UploadFile,
    Getobject,
    DeleteFile,
    UploadFileToFolder,
    UploadBase64Format
}