var express = require('express');
var router = express.Router();

//import modules for calling in end points 
const client = require('./client');
const lookup = require('./lookup');

//use routers for calling function end points 
router.use('/client', client);
router.use('/lookup', lookup);

module.exports = router;


