const express = require('express');
var router = express.Router();
const clientValidator = require(`../validation/client`);

/**
 * get function to get all clients
 */
 router.get(`/`, async function (req, res, next) {
    console.log("Hello-get");
    return res.sendStatus(200);
});

/**
 * post function to create user clients
 */
router.post(`/`, clientValidator.validationPostClient, async function (req, res, next) {
    console.log("Hello-post");
    return res.sendStatus(201);
});

/**
* put function for client details
*/
router.put(`/`, async function (req, res, next) {
    console.log("Hello-put");
    return res.sendStatus(200);
});

/**
* function to change status of client
*/
router.put(`/status`, async function (req, res, next) {
    console.log("Hello-status");
    return res.sendStatus(200);
});

module.exports = router;