var express = require('express');
var router = express.Router();
var lookup_helper = require('../controller/client/lookup');
var schema_validation = require('../validation/client')


router.get('/', schema_validation.getLookupValidation, async function (req, res, next) {
    /**
      * get lookup details
      *  @param {query} key
      */

    var key = req.query.key;

    await lookup_helper.getLookup(
        key
    ).then(function (response) {
        res.status(200).send(response)
    }).catch(function (error) {
        res.status(500).send({ "error": error })
    })

})

module.exports = router;