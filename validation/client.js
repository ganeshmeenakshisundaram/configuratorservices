var Validator = require("jsonschema").Validator;

/**
 * validation for post of clinet 
 */
async function validationPostClient(req, res, next) {
    next();
}

async function getLookupValidation(req, res, next) {
    /**
     * validation of description
     *  @param {query} key
     */
  
    // requesting event_id from query
    var req_key = req.query.key;
    // if event_id exists in the query(condtion)
    if (req_key && (typeof req_key === "string" || req_key instanceof String)) {
      var splited_value = req_key.split(",");
      var allowedLookups = [
        "country",
        "state",
        "city",
        "type_of_hash",
        "common_date_formate",
        "time_zone",
        "client_status"
      ];
      splited_value.forEach((element) => {
        //for loop is taken fecth table data into array
        if (allowedLookups.includes(element)) {
          return true;
        } else {
          return res.status(401).send({ error: "KEY_IS_NOT_VALID" });
        }
      });
      next();
    } else {
      return res.status(401).send({ error: "REQUIRED_KEY" });
    }
  }

module.exports = {
    validationPostClient,
    getLookupValidation
}