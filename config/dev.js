var dev = {
    "dev": {
      "name": "Hypermatrix - Dev Mode",
      "port": process.env.DEV_APP_PORT,
      "mode": "development",
      "protocol": "http",
      "serverUrl": "localhost",
      "serverUrlWebUrlLink": `http://localhost:${process.env.DEV_APP_PORT}/`,
      "database": {
        "multipleStatements": true,
        "host": process.env.DEV_DB_HOST,
        "port": process.env.DEV_DB_PORT,
        "user": process.env.DEV_DB_USERNAME,
        "password": process.env.DEV_DB_PASSWORD,
        "database": process.env.DEV_DB_NAME,
      },
      "email": {
        "host": "",
        "port": "",
        "username": "",
        "password": ""
      }
    }
  }
  
  module.exports = dev;
  
  