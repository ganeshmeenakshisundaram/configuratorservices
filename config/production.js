var production = {
    "production": {
      "name": "Hypermatrix - Production Mode",
      "port": process.env.PROD_APP_PORT,
      "mode": "development",
      "protocol": "http",
      "serverUrl": "localhost",
      "serverUrlWebUrlLink": `http://localhost:${process.env.PROD_APP_PORT}/`,
      "database": {
        "multipleStatements": true,
        "host": process.env.PROD_DB_HOST,
        "port": process.env.PROD_DB_PORT,
        "user": process.env.PROD_DB_USERNAME,
        "password": process.env.PROD_DB_PASSWORD,
        "database": process.env.PROD_DB_NAME,
      },
      "email": {
        "host": "",
        "port": "",
        "username": "",
        "password": ""
      }
    }
  }
  
  module.exports = production;