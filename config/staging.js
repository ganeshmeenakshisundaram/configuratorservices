var staging = {
    "staging": {
      "name": "Hypermatrix - Staging Mode",
      "port": process.env.STAG_APP_PORT,
      "mode": "development",
      "protocol": "http",
      "serverUrl": "localhost",
      "serverUrlWebUrlLink": `localhost:${process.env.STAG_APP_PORT}/`,
      "database": {
        "multipleStatements": true,
        "host": process.env.STAG_DB_HOST,
        "port": process.env.STAG_DB_PORT,
        "user": process.env.STAG_DB_USERNAME,
        "password": process.env.STAG_DB_PASSWORD,
        "database": process.env.STAG_DB_NAME,
      },
      "email": {
        "host": "",
        "port": "",
        "username": "",
        "password": ""
      }
    }
  }
  
  module.exports = staging;
  
  