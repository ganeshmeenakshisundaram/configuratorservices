const sequelize = require('../middlewear/sequelize');
const { DataTypes } = require('sequelize');

const User = sequelize.define('TestOrm', {
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true
      },
    // Model attributes are defined here
    name: {
      type: DataTypes.STRING,
      allowNull: false
    },
    mobile: {
      type: DataTypes.STRING
      // allowNull defaults to true
    }
}, {
    tableName: 'test_orm',
    timestamps: false
});

module.exports = User